const members = [{
        id: 1,
        name: 'Bjorn James',
        email: 'bvferrer@epdsi.ph',
        status: 'active'
    },
    {
        id: 2,
        name: 'Maria Fe',
        email: 'mariafe@gmail.com',
        status: 'inactive'
    },
    {
        id: 3,
        name: 'Maliya Ferrer',
        email: 'maliyaferrer@gmail.com',
        status: 'active'
    },
];

module.exports = members;